import os

ROOT_FOLDER = os.path.dirname(os.path.abspath(__file__))
PROJ_FOLDER = os.path.join(ROOT_FOLDER, '..')
DATA_DIR = os.path.join(PROJ_FOLDER, 'data')
